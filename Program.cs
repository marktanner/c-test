﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace test
{
    static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            List<string> words = new List<string>
            {
                "restful",
                "flow",
                "fluster",
                "wolf",
                "floww",
            };

            /// remove anagrams from the given list, keeping only the first instance of each 'unique' word
            /// the returned list should also be sorted using the normal string comparer
            /// in the above list: 'restful' will be kept but 'fluster' will be removed as it is an anagram of 'restful'
            List<string> actualResult = RemoveAnagrams(words);

            List<string> expectedResult = new List<string>
            {
                "flow",
                "floww",
                "restful"
            };

            Console.WriteLine(Enumerable.SequenceEqual(expectedResult, actualResult));
        }

        static List<string> RemoveAnagrams(List<string> words)
        {
            throw new NotImplementedException();
        }
    }
}
